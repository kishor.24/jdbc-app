package demo2;

import java.util.List;

public class Jdbc02Main {
	public static void main(String[] args) {
		// Get all books
		//getAllBooks();
		
		// Insert a book
		// insertBook();
		
		// delete a book
		// deleteBook();
		
		// Get a book
		//getBook();
		
		// Get all books with same subject
		// getAllBooksBySubject();
		
		// Get all subjects
		getAllSubjects();
	}
	
	/**
	 * Get All books
	 * 
	 */
	public static void getAllBooks() {
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			List<Book> list = dao.findAllBooks();
			for (Book b : list)
				System.out.println(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Add new book
	 * 
	 */
	public static void insertBook() {
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			int cnt = dao.addBook(new Book(51, "Atlas Shrugged", "Ayn Rand", "Novell", 534.76));
			System.out.println("Books inserted: " + cnt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete a book
	 * 
	 */
	public static void deleteBook() {
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			int status = dao.deleteBook(51);
			System.out.println(status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get a book
	 * 
	 */
	public static void getBook() {
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			Book book = dao.findById(11);
			System.out.println(book);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get books by subject
	 * 
	 */
	public static void getAllBooksBySubject() {
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			List<Book> list = dao.findBySubject("C");
			for (Book b : list)
				System.out.println(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get all the available subjects
	 */
	public static void getAllSubjects() {
		try(BookDaoImpl dao = new BookDaoImpl()) {
			dao.open();
			List<String> list = dao.findSubjects();
			for (String subject : list)
				System.out.println(subject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
